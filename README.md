# Code experimentation, R & D and stuff

This repository is a pool of visual experimentations or modules coded for web. They may rely on libraries such as [ThreeJS](https://github.com/mrdoob/three.js/) or [PixiJS](https://github.com/GoodBoyDigital/pixi.js/).

## Setup

### NodeJS
Be sure you have a recent installation of [nodejs](http://nodejs.org/).

### Gulp
This project relies on [gulp](http://gulpjs.com/) and several of its plugins.

Run `npm install` to install node dependencies modules.

## Getting started
Run `gulp --name webgl-gpgpu-particles` to compile, watch and preview module.  
Run `gulp build --name webgl-gpgpu-particles --production` to compile for production.  