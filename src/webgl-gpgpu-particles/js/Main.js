import Particles from "./particles/Particles";
import Webcam from "lib/anonymous/components/webcam/Webcam";
import AModule from "modules/AModule";


/**
 * Main
 * @constructor
 */
export default class Main extends AModule
{
	constructor()
	{
		super();
	
		this.init();
	}

	init()
	{
		super.init(this);
		
		// 
		this._content = $("#particles");

		// particles
		this._particles = new Particles(this._content[0]);

		// webcam
		this._webcam = new Webcam(
			{
				dom: document.querySelector(".source__video"),
				video: true,
				audio: false
			});
		this._webcam.$.on("error", this._onWebcamError.bind(this));
		this._webcam.$.on("success", this._onWebcamSuccess.bind(this));
	}

	/**
	 * Drawing on requestAnimationFrame
	 */
	update()
	{
		this._particles.update();

		super.update();
	}

	/**
	 * Triggered on window resize
	 */
	_onResize()
	{
		super._onResize(this);

		this._particles.resize();
	}

	//-----------------------------------------------------o webcam handlers

	_onWebcamError()
	{
		// No webcam detected, image fallback
		const img = document.createElement("img");
		img.addEventListener("load", () => {
			this._particles.setTexture(img, false, false);
		});
		img.style.position = "absolute";
		img.style.bottom = "100%";
		img.style.right = "100%";
		img.src = 'img/portrait.jpg';
		// img.src = 'img/hello.jpg';
		document.body.appendChild(img);
	}

	_onWebcamSuccess()
	{
		// set input texture, could be video / image (will be drawn in a canvas)
		this._particles.setTexture(this._webcam.dom, true, true);
	}
}

/**
 * Let's roll
 */
const onDomContentLoaded = function() 
{
	document.removeEventListener("DOMContentLoaded", onDomContentLoaded);

	const main = new Main();

	(function tick()
	{
		main.update();
		window.requestAnimationFrame(tick);
	})();
};
document.addEventListener("DOMContentLoaded", onDomContentLoaded);