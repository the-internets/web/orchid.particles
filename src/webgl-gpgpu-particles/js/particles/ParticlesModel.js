import * as dat from 'dat.gui';

/**
 * Controller
 * @constructor
 */
export default class ParticlesModel
{
	constructor()
	{
		this.init();
	}

	init() 
	{
		this.data = 
		{
			// texture
			threshold: 0,
			smoothness: 1,
			strength: 0.32,
			
			// look & feel
			initialAttraction: 0,
			frictions: 0.0,
			resetStacked: true,
			stackSensibility: 0.18,

			velocityMax: 0.00026,
			mapStrength: 0.0003,
			pointSize: 2,
			density: 1.6,
			alpha: 0.60,
			inverted: false,
			particlesColor: "#000000",
			bgColor: "#FFFFFF",
		};

		var callback = $.proxy(this._onChange, this);

		this._gui = new dat.GUI();
		// this._gui.close();

		this._gui.add(this.data, 'initialAttraction', 0, 0.3).step(0.0001).onChange(callback);
		this._gui.add(this.data, 'frictions', 0, 0.5).step(0.001).onChange(callback);
		this._gui.add(this.data, 'velocityMax', 0, 0.02).step(0.00001).onChange(callback);
		this._gui.add(this.data, 'pointSize', 1, 15).step(1).onChange(callback);
		this._gui.add(this.data, 'alpha', 0, 1).step(0.0001).onChange(callback);
		this._gui.add(this.data, 'density', 1, 10).step(0.001).onChange(callback);
		this._gui.add(this.data, 'mapStrength', 0, 0.2).step(0.0001).onChange(callback);
		
		this._gui.add(this.data, 'resetStacked').onChange(callback);
		this._gui.add(this.data, 'stackSensibility', 0, 3).step(0.00001).onChange(callback);

		this._gui.add(this.data, 'inverted').onChange(callback);
		this._gui.addColor(this.data, 'particlesColor').onChange(callback);
		this._gui.addColor(this.data, 'bgColor').onChange(callback);
	}

	_onChange()
	{
		$(this).trigger("change");
	}
}