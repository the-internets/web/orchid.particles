uniform vec3 uColor;
uniform float uAlpha;
uniform float uDensity;
uniform float uPointSize;

varying vec4 vColor;

void main()
{
	float luminance = 0.2126 * vColor.r + 0.7152 * vColor.g + 0.0722 * vColor.b;

	vec2 center = (gl_PointCoord.xy - 0.5) * 2.0;
	float l = pow(length(center), uDensity);
	float s = step(2., uPointSize);
	float a = ((1. - s) + s * clamp(1.0 - l, 0., 1.)) * uAlpha * (1. - luminance);

	if(a < 0.001) discard;

	gl_FragColor = vec4(uColor, a);
}