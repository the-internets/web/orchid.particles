uniform sampler2D uTexturePosition;
uniform sampler2D uTexture;
uniform float uPointSize;

varying vec4 vColor;

void main()
{
	vec2 uv = position.xy;
	vec4 tex = texture2D(uTexturePosition, uv);

	vColor = texture2D(uTexture, tex.xy);
	
	gl_PointSize = uPointSize;
	gl_Position = vec4(tex.xy * 2.0 - 1.0, 0.0, 1.0);
}